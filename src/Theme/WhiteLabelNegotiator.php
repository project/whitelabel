<?php

namespace Drupal\whitelabel\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\whitelabel\WhiteLabelManagerInterface;

/**
 * Registers the configured white label theme as the active theme.
 */
class WhiteLabelNegotiator implements ThemeNegotiatorInterface {

  /**
   * The white label manager.
   *
   * @var \Drupal\whitelabel\WhiteLabelManagerInterface
   */
  protected $whiteLabelManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * WhiteLabelNegotiator constructor.
   *
   * @param \Drupal\whitelabel\WhiteLabelManagerInterface $white_label_manager
   *   The white label manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(WhiteLabelManagerInterface $white_label_manager, ConfigFactoryInterface $config_factory) {
    $this->whiteLabelManager = $white_label_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    // Only apply if there is an active white label.
    if ($this->whiteLabelManager->getWhiteLabel()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $fixed_theme = $this->configFactory->get('whitelabel.settings')->get('site_fixed_theme');
    $allow_overrides = $this->configFactory->get('whitelabel.settings')->get('site_theme');

    $theme = $this->whiteLabelManager->getWhiteLabel()->getTheme();

    // If users have permissions to set their own themes.
    if ($allow_overrides && !empty($theme)) {
      // Return the theme configured for the white label.
      return $theme;
    }
    elseif (!empty($fixed_theme)) {
      // If a fixed theme was set, use that.
      return $fixed_theme;
    }

    // No user specific config and no global setting.
    // Allow other negotiators to resolve this.
    return NULL;
  }

}
