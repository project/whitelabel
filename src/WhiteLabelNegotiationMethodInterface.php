<?php

namespace Drupal\whitelabel;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface for white label negotiation classes.
 */
interface WhiteLabelNegotiationMethodInterface {

  /**
   * Returns the label for use on the administration pages.
   *
   * @return string
   *   The administration label.
   */
  public function label();

  /**
   * Returns the plugin's description.
   *
   * @return string
   *   A string describing the plugin. Might contain HTML and should be already
   *   sanitized for output.
   */
  public function getDescription();

  /**
   * Set the white label manager.
   *
   * @param \Drupal\whitelabel\WhiteLabelManagerInterface $manager
   *   The white label manager.
   */
  public function setWhiteLabelManager(WhiteLabelManagerInterface $manager);

  /**
   * Performs white label negotiation.
   *
   * @param \Symfony\Component\HttpFoundation\Request|null $request
   *   (optional) The current request. Defaults to NULL if it has not been
   *   initialized yet.
   *
   * @return \Drupal\whitelabel\Entity\WhiteLabelInterface
   *   A valid white label or FALSE if the negotiation was unsuccessful.
   */
  public function getWhiteLabel(Request $request = NULL);

}
