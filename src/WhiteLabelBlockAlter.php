<?php

namespace Drupal\whitelabel;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Render\Element\RenderCallbackInterface;

/**
 * Provides a trusted callback to alter blocks depending on given config tags.
 *
 * @see whitelabel_block_view_alter()
 */
class WhiteLabelBlockAlter implements RenderCallbackInterface {

  /**
   * Pre-render callback: Sets white label cacheability metadata on blocks.
   *
   * Scans all blocks to see if they depend on the system.site cache tag. If so,
   * also make them depend on the white label cache tags and cache context to
   * make sure we can cache one block for every white label.
   */
  public static function preRender($build) {
    // Only act if the build already contained a cacheable dependency on the
    // system.site cache tag.
    $cacheable_metadata = CacheableMetadata::createFromRenderArray($build);
    $tags = $cacheable_metadata->getCacheTags();
    if (in_array('config:system.site', $tags)) {
      // Always create a cache context for the no-whitelabel version.
      $cacheable_metadata->addCacheContexts(['whitelabel']);

      if ($whitelabel = \Drupal::service('whitelabel.whitelabel_manager')->getWhiteLabel()) {
        // Add white label specific cacheability metadata.
        $cacheable_metadata->addCacheableDependency($whitelabel);
      }
      // Add cacheable metadata.
      $cacheable_metadata->applyTo($build);
    }
    return $build;
  }

}
