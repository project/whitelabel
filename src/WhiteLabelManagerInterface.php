<?php

namespace Drupal\whitelabel;

use Drupal\whitelabel\Entity\WhiteLabelInterface;

/**
 * The interface for white label managers.
 */
interface WhiteLabelManagerInterface {

  /**
   * Fetch the white label.
   *
   * @return \Drupal\whitelabel\Entity\WhiteLabelInterface|null
   *   The found white label, or NULL of none was found.
   */
  public function getWhiteLabel();

  /**
   * Set the white label.
   *
   * @param \Drupal\whitelabel\Entity\WhiteLabelInterface $white_label
   *   The white label.
   */
  public function setWhiteLabel(WhiteLabelInterface $white_label);

  /**
   * Unset the white label.
   */
  public function unsetWhiteLabel();

}
