<?php

namespace Drupal\whitelabel;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\whitelabel\Entity\WhiteLabelInterface;

/**
 * Sets and unsets white labels that were negotiated by the plugins.
 */
class WhiteLabelManager implements WhiteLabelManagerInterface {

  /**
   * The white label negotiator.
   *
   * @var \Drupal\whitelabel\WhiteLabelNegotiatorInterface
   */
  protected $negotiator;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The white label.
   *
   * @var \Drupal\whitelabel\Entity\WhiteLabelInterface
   */
  protected $whiteLabel;

  /**
   * Constructs a new WhiteLabelManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function setNegotiator(WhiteLabelNegotiatorInterface $negotiator) {
    $this->negotiator = $negotiator;
  }

  /**
   * Searches for a white label with a given token.
   *
   * @param string $token
   *   The token to search for.
   *
   * @return \Drupal\whitelabel\Entity\WhiteLabelInterface|null
   *   The white label or NULL if none was found.
   */
  public function getWhiteLabelByToken($token) {
    if (empty($token)) {
      return NULL;
    }

    /** @var \Drupal\whitelabel\Entity\WhiteLabelInterface[] $whitelabels */
    if ($whitelabels = $this->entityTypeManager->getStorage('whitelabel')->loadByProperties(['token' => $token])) {
      return reset($whitelabels);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getWhiteLabel() {
    if (empty($this->whiteLabel)) {
      $whiteLabel = $this->negotiator->init();

      if ($whiteLabel) {
        $this->setWhiteLabel($whiteLabel);
      }
    }

    // Return stored value or do negotiation.
    return $this->whiteLabel ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setWhiteLabel(WhiteLabelInterface $white_label) {
    $this->whiteLabel = $white_label;
  }

  /**
   * {@inheritdoc}
   */
  public function unsetWhiteLabel() {
    $this->whiteLabel = NULL;
  }

}
