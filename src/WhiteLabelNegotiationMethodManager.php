<?php

namespace Drupal\whitelabel;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manager for White label negotiation plugins.
 */
class WhiteLabelNegotiationMethodManager extends DefaultPluginManager {

  /**
   * Constructs a new WhiteLabelNegotiationMethodManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   An object that implements ModuleHandlerInterface.
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/WhiteLabelNegotiation',
      $namespaces,
      $module_handler,
      'Drupal\whitelabel\WhiteLabelNegotiationMethodInterface',
      'Drupal\whitelabel\Annotation\WhiteLabelNegotiation'
    );
    $this->alterInfo('whitelabel_negotiation_info');
  }

  /**
   * {@inheritdoc}
   */
  public function findDefinitions() {
    $definitions = parent::findDefinitions();
    // Load the weights from the config.
    $config = \Drupal::config('whitelabel.negotiation');
    foreach ($definitions as $key => &$definition) {
      $weight = $config->get("negotiator_settings.$key.weight") ?? NULL;
      $definition['weight'] = $weight;
    }
    // Sort the plugins by weight.
    uasort($definitions, ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);
    return $definitions;
  }

}
