<?php

namespace Drupal\whitelabel;

use Drupal\Component\Plugin\PluginManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The default White label negotiator.
 */
class WhiteLabelNegotiator implements WhiteLabelNegotiatorInterface {

  /**
   * The white label manager.
   *
   * @var \Drupal\whitelabel\WhiteLabelManagerInterface
   */
  protected $whiteLabelManager;

  /**
   * The white label negotiation method plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $negotiatorManager;

  /**
   * The request stack object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Local cache for white label negotiation method instances.
   *
   * @var \Drupal\whitelabel\WhiteLabelNegotiationMethodInterface[]
   */
  protected $methods;

  /**
   * Construct a WhiteLabelNegotiator.
   */
  public function __construct(WhiteLabelManagerInterface $whitelabel_manager, PluginManagerInterface $negotiator_manager, RequestStack $requestStack) {
    $this->whiteLabelManager = $whitelabel_manager;
    $this->negotiatorManager = $negotiator_manager;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public function initWhiteLabelManager() {
    $this->whiteLabelManager->setNegotiator($this);
  }

  /**
   * {@inheritdoc}
   */
  public function reset() {
    $this->methods = [];
  }

  /**
   * {@inheritdoc}
   */
  public function init() {
    $white_label = NULL;

    // Iterate over the negotiators until one returns a white label.
    foreach ($this->getNegotiationMethods() as $method_id => $type) {
      if (!isset($white_label)) {
        $white_label = $this->negotiateWhiteLabel($method_id);
      }
      if ($white_label) {
        return $white_label;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function negotiateWhiteLabel($method_id) {
    $white_label = $this->getNegotiationMethodInstance($method_id)->getWhiteLabel($this->requestStack->getCurrentRequest());
    return $white_label ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getNegotiationMethods() {
    $definitions = $this->negotiatorManager->getDefinitions();

    $config = \Drupal::config('whitelabel.negotiation');
    foreach ($definitions as $method_id => $definition) {
      $enabled = $config->get("negotiator_settings.$method_id.enabled") ? TRUE : FALSE;
      if (!$enabled) {
        unset ($definitions[$method_id]);
      }
    }

    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getNegotiationMethodInstance($method_id) {
    $config = \Drupal::config('whitelabel.negotiation');
    $plugin_config = $config->get("negotiator_settings.$method_id.settings") ?? [];

    if (!isset($this->methods[$method_id])) {
      $instance = $this->negotiatorManager->createInstance($method_id, $plugin_config);
      $instance->setWhiteLabelManager($this->whiteLabelManager);
      $this->methods[$method_id] = $instance;
    }
    return $this->methods[$method_id];
  }

}
