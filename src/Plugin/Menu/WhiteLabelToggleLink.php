<?php

namespace Drupal\whitelabel\Plugin\Menu;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\Core\Menu\StaticMenuLinkOverridesInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\whitelabel\WhiteLabelManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Menu link.
 */
class WhiteLabelToggleLink extends MenuLinkDefault {

  /**
   * The white label manager.
   *
   * @var \Drupal\whitelabel\WhiteLabelManagerInterface
   */
  protected $whiteLabelManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new WhiteLabelToggleLink.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\StaticMenuLinkOverridesInterface $static_override
   *   The static override storage.
   * @param \Drupal\whitelabel\WhiteLabelManagerInterface $white_label_manager
   *   The white label manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StaticMenuLinkOverridesInterface $static_override, WhiteLabelManagerInterface $white_label_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $static_override);

    $this->whiteLabelManager = $white_label_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu_link.static.overrides'),
      $container->get('whitelabel.whitelabel_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Check if the current user does have an associated white label.
   *
   * Returns the first found white label.
   */
  private function userHasWhiteLabel(AccountInterface $account = NULL) {
    if (!$account) {
      $account = \Drupal::currentUser();
    }

    if ($whitelabels = $this->entityTypeManager->getStorage('whitelabel')
      ->loadByProperties(['uid' => $account->id()])) {
      return reset($whitelabels);
    }
    return NULL;
  }

  // public function isEnabled() {
  //   return (bool) $this->userHasWhiteLabel();
  // }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    if ($this->whiteLabelManager->getWhiteLabel()) {
      return $this->t('@site_name site', ['@site_name' => \Drupal::configFactory()->getEditable('system.site')->get('name')]);
    }
    else {
      return $this->t('My site');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {
    $options = parent::getOptions();

    if ($this->whiteLabelManager->getWhiteLabel()) {
      // Explicitly unset the white label.
      $options['whitelabel'] = FALSE;
    }
    else {
      // Explicitly set the white label. The outbound url processor is also
      // reading the white label option to apply it in the right place.
      $options['whitelabel'] = $this->userHasWhiteLabel();
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['whitelabel'];
  }

}
