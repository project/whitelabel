<?php

namespace Drupal\whitelabel\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Drupal\whitelabel\whiteLabelManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default argument plugin to extract the current uid from a given white label.
 *
 * This plugin can be used to populate a user id field from a white label.
 *
 * @ViewsArgumentDefault(
 *   id = "whitelabel_current",
 *   title = @Translation("White Label ID of the active White Label")
 * )
 */
class CurrentWhiteLabel extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * Holds the white label provider.
   *
   * @var \Drupal\whitelabel\whiteLabelManagerInterface
   */
  protected $whiteLabelManager;

  /**
   * Constructs a new Node instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\whitelabel\WhiteLabelManagerInterface $white_label_manager
   *   The white label manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, WhiteLabelManagerInterface $white_label_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->whiteLabelManager = $white_label_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('whitelabel.whitelabel_manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    // If there is a whitelabel, return its owner.
    $whitelabel = $this->whiteLabelManager->getWhiteLabel();
    return !empty($whitelabel) ? $whitelabel->id() : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['whitelabel'];
  }

}
