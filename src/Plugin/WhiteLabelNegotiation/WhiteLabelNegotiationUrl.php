<?php

namespace Drupal\whitelabel\Plugin\WhiteLabelNegotiation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\whitelabel\WhiteLabelNegotiationMethodBase;
use Drupal\whitelabel\WhiteLabelNegotiationMethodInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class for identifying white labels via URL.
 *
 * @WhiteLabelNegotiation(
 *   id = \Drupal\whitelabel\Plugin\WhiteLabelNegotiation\WhiteLabelNegotiationUrl::METHOD_ID,
 *   label = @Translation("URL"),
 *   description = @Translation("White label from the URL."),
 *   weight = -10,
 * )
 */
class WhiteLabelNegotiationUrl extends WhiteLabelNegotiationMethodBase implements WhiteLabelNegotiationMethodInterface, InboundPathProcessorInterface, OutboundPathProcessorInterface {

  use StringTranslationTrait;

  /**
   * The white label negotiation method id.
   */
  const METHOD_ID = 'url';

  /**
   * White label negotiation: use a query parameter as whitelabel indicator.
   */
  const CONFIG_QUERY_PARAMETER = 'query_parameter';

  /**
   * White label negotiation: use the path prefix as whitelabel indicator.
   */
  const CONFIG_PATH_PREFIX = 'path_prefix';

  /**
   * White label negotiation: use the domain as whitelabel indicator.
   */
  const CONFIG_DOMAIN = 'domain';

  /**
   * Helper function for fetching all white label modes.
   *
   * @return string[]
   *   An array of descriptions, keyed by the mode system name.
   */
  public static function getModes() {
    return [
      self::CONFIG_QUERY_PARAMETER => t('Query parameter (<em>example.com/page?token=<strong>whitelabel_token</strong></em>)'),
      self::CONFIG_DOMAIN => t('Domain (<em><strong>whitelabel_token</strong>.example.com/page</em>)'),
      self::CONFIG_PATH_PREFIX => t('Path prefix (<em>example.com/<strong>whitelabel_token</strong>/page</em>)'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'mode' => self::CONFIG_QUERY_PARAMETER,
      'query_string_identifier' => 'store',
      'domain' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getWhiteLabel(Request $request = NULL) {
    $whitelabel_mode = $this->configuration['mode'];

    $token = NULL;
    $whitelabel = NULL;
    $invalid_whitelabel = FALSE;

    switch ($whitelabel_mode) {
      case self::CONFIG_QUERY_PARAMETER:
        $query_string_identifier = $this->configuration['query_string_identifier'];
        $token = $request->query->get($query_string_identifier) ?: NULL;

        // Try to load the white label from the query parameter.
        $whitelabel = $this->whiteLabelManager->getWhiteLabelByToken($token);
        break;

      case self::CONFIG_PATH_PREFIX:
        $request_path = urldecode(trim($request->getPathInfo(), '/'));
        $path_args = explode('/', $request_path);
        $token = array_shift($path_args);

        // Rebuild a path with the remaining parts.
        $temp_path = implode('/', $path_args);

        $whitelabel = $this->whiteLabelManager->getWhiteLabelByToken($token);

        // This has toe be done like this to prevent circular references.
        $path_validator = \Drupal::service('path.validator');

        // White label found and valid remaining path.
        if ($whitelabel && $path_validator->isValid($temp_path)) {
          $path = $temp_path;
        }
        // No white label and a valid initial path; treat this as a no
        // white label request.
        elseif (empty($whitelabel) && $path_validator->isValid($request_path)) {
          return NULL;
        }
        // In all other cases there is something wrong.
        else {
          $invalid_whitelabel = TRUE;
        }
        break;

      case self::CONFIG_DOMAIN:
        // Get only the host, not the port.
        $http_host = $request->getHost();
        $host_parts = explode('.', $http_host);

        // Make sure that the host was the token plus the base domain.
        if ($http_host == $host_parts[0] . '.' . $this->configuration['domain'] && $whitelabel = $this->whiteLabelManager->getWhiteLabelByToken($host_parts[0])) {
          $token = $host_parts[0];
        }
        else {
          $invalid_whitelabel = TRUE;
        }
        break;
    }

    // Return the white label if there is one.
    if (!empty($whitelabel)) {
      return $whitelabel;
    }

    // If the white label is invalid or permissions are wrong, show 404.
    if ($invalid_whitelabel && ($whitelabel_mode == self::CONFIG_PATH_PREFIX || $whitelabel_mode == self::CONFIG_DOMAIN)) {
      throw new NotFoundHttpException(sprintf("The path '%s' and the discovered white label '%s' did not result in an existing page.", $path, $token));
    }

    // In all other cases no white label was resolved.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    $whitelabel_mode = $this->configuration['mode'];

    if ($whitelabel_mode == self::CONFIG_PATH_PREFIX) {
      $parts = explode('/', trim($path, '/'));
      $prefix = array_shift($parts);

      // Search prefix within white labels.
      if ($this->whiteLabelManager->getWhiteLabelByToken($prefix)) {
        // Rebuild $path with the white label removed.
        $path = '/' . implode('/', $parts);
      }
    }

    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    // Get the active white label.
    $whitelabel = $this->whiteLabelManager->getWhiteLabel();

    // No white label or no permission to serve it, so leave.
    if (empty($whitelabel)) {
      // Also add a cache context for no white label requests.
      if ($bubbleable_metadata) {
        $bubbleable_metadata->addCacheContexts(['whitelabel']);
      }
      return $path;
    }

    $whitelabel_token = $whitelabel->getToken();

    // Apply white label in the right place.
    $whitelabel_mode = $this->configuration['mode'];
    switch ($whitelabel_mode) {
      case self::CONFIG_QUERY_PARAMETER:
        // Append the white label query parameter.
        $query_string_identifier = $this->configuration['query_string_identifier'];
        $options['query'][$query_string_identifier] = $whitelabel_token;
        break;

      case self::CONFIG_PATH_PREFIX:
        // Append the white label token as a prefix, preserve existing prefixes.
        // The weight of the inbound path processors defines the inbound order.
        // (So make sure they match.)
        $options['prefix'] = $whitelabel_token . '/' . $options['prefix'];
        break;

      case self::CONFIG_DOMAIN:
        // Append the white label token as a domain prefix.
        $url_scheme = 'http';
        $port = 80;
        if ($request) {
          $url_scheme = $request->getScheme();
          $port = $request->getPort();
        }

        global $base_url;
        $options['base_url'] = $options['base_url'] ?? $base_url;

        // Save the original base URL. If it contains a port, we need to
        // retain it below.
        if (!empty($options['base_url'])) {
          // The colon in the URL scheme messes up the port checking below.
          $normalized_base_url = str_replace([
            'https://',
            'http://',
          ], '', $options['base_url']);
        }

        // Ask for an absolute URL with our modified base URL.
        $options['absolute'] = TRUE;

        $options['base_url'] = $url_scheme . '://' . $whitelabel_token . '.' . $normalized_base_url;

        // In case either the original base URL or the HTTP host contains a
        // port, retain it.
        if (isset($normalized_base_url) && strpos($normalized_base_url, ':') !== FALSE) {
          [, $port] = explode(':', $normalized_base_url);
          $options['base_url'] .= ':' . $port;
        }
        elseif (($url_scheme == 'http' && $port != 80) || ($url_scheme == 'https' && $port != 443)) {
          $options['base_url'] .= ':' . $port;
        }

        if (isset($options['https'])) {
          if ($options['https'] === TRUE) {
            $options['base_url'] = str_replace('http://', 'https://', $options['base_url']);
          }
          elseif ($options['https'] === FALSE) {
            $options['base_url'] = str_replace('https://', 'http://', $options['base_url']);
          }
        }

        // Add Drupal's sub-folder from the base_path if there is one.
        $options['base_url'] .= rtrim(base_path(), '/');
        break;
    }

    if ($bubbleable_metadata) {
      $bubbleable_metadata->addCacheableDependency($whitelabel);
    }

    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $change_warning = $this->t('WARNING: CHANGING THIS DURING PRODUCTION WILL CAUSE EXISTING WHITE LABEL LINKS TO BREAK.');

    $form['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Detection mode'),
      '#default_value' => $this->configuration['mode'],
      '#description' => $this->t('The domain mode requires a white label DNS record and optionally a white label SSL certificate. <br>@warning', ['@warning' => $change_warning]),
      '#options' => self::getModes(),
    ];

    $form['query_string_identifier'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Query string identifier'),
      '#default_value' => $this->configuration['query_string_identifier'],
      '#description' => $this->t("When using a query string, this defines the parameter to use. In the following example %example, the query string parameter would be 'token'. <br>@warning", ['%example' => self::getModes()[self::CONFIG_QUERY_PARAMETER], '@warning' => $change_warning]),
      '#machine_name' => [
        'exists' => [$this, 'validQueryString'],
        'standalone' => TRUE,
      ],
      '#states' => [
        'visible' => [
          ':input[name="negotiators[' . self::METHOD_ID . '][settings][mode]"]' => ['value' => self::CONFIG_QUERY_PARAMETER],
        ],
        'required' => [
          ':input[name="negotiators[' . self::METHOD_ID . '][settings][mode]"]' => ['value' => self::CONFIG_QUERY_PARAMETER],
        ],
      ],
    ];

    $form['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base domain'),
      '#default_value' => $this->configuration['domain'],
      '#description' => $this->t('When using subdomain-mode, this is used to determine the base domain.'),
      '#states' => [
        'visible' => [
          ':input[name="negotiators[' . self::METHOD_ID . '][settings][mode]"]' => ['value' => self::CONFIG_DOMAIN],
        ],
        'required' => [
          ':input[name="negotiators[' . self::METHOD_ID . '][settings][mode]"]' => ['value' => self::CONFIG_DOMAIN],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * Helper function for detecting if the provided query string is valid.
   *
   * @param string $id
   *   The ID to check.
   *
   * @return bool
   *   Boolean to indicate if the query string is already in use.
   */
  public function validQueryString($id) {
    // @todo ensure this actually checks something.
    return FALSE;
  }

}
