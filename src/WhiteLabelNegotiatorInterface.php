<?php

namespace Drupal\whitelabel;

/**
 * Interface for white label negotiators.
 *
 * @package Drupal\whitelabel
 */
interface WhiteLabelNegotiatorInterface {

  /**
   *
   */
  public function initWhiteLabelManager();

  /**
   *
   */
  public function reset();

  /**
   *
   */
  public function init();

  /**
   *
   */
  public function negotiateWhiteLabel($method_id);

  /**
   *
   */
  public function getNegotiationMethods();

  /**
   *
   */
  public function getNegotiationMethodInstance($method_id);

}
