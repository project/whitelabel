<?php

namespace Drupal\whitelabel\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a white label negotiation annotation object.
 *
 * Plugin Namespace: Plugin\WhiteLabelNegotiation.
 *
 * For a working example, see
 * \Drupal\whitelabel\Plugin\WhiteLabelNegotiation\WhiteLabelNegotiationUrl.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class WhiteLabelNegotiation extends Plugin {

  /**
   * The white label negotiation plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The white label negotiation plugin label.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The white label negotiation plugin description.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The plugin weight.
   *
   * @var int
   */
  public $weight;

}
