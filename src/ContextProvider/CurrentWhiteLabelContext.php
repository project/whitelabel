<?php

namespace Drupal\whitelabel\ContextProvider;

use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\whitelabel\WhiteLabelManagerInterface;

/**
 * Sets the current white label as a context.
 */
class CurrentWhiteLabelContext implements ContextProviderInterface {

  use StringTranslationTrait;

  /**
   * The current white label.
   *
   * @var \Drupal\whitelabel\WhiteLabelManagerInterface
   */
  protected $whiteLabelManager;

  /**
   * CurrentWhiteLabelContext constructor.
   *
   * @param \Drupal\whitelabel\WhiteLabelManagerInterface $white_label_manager
   *   The white label manager.
   */
  public function __construct(WhiteLabelManagerInterface $white_label_manager) {
    $this->whiteLabelManager = $white_label_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getRuntimeContexts(array $unqualified_context_ids) {
    $result = [];
    $context_definition = EntityContextDefinition::create('whitelabel')->setRequired(FALSE);
    $value = NULL;

    if ($white_label = $this->whiteLabelManager->getWhiteLabel()) {
      $value = $white_label;
    }

    $context = new Context($context_definition, $value);
    if ($white_label) {
      $context->addCacheableDependency($white_label);
    }
    $result['whitelabel'] = $context;

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableContexts() {
    $context = EntityContext::fromEntityTypeId('whitelabel', $this->t('Active white label'));
    return ['whitelabel' => $context];
  }

}
