<?php

namespace Drupal\whitelabel\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\whitelabel\WhiteLabelNegotiationMethodManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for showing all White label negotiators and their options.
 *
 * @package Drupal\whitelabel\Form
 */
class NegotiationConfigureForm extends ConfigFormBase {

  /**
   * The white label negotiation manager.
   *
   * @var \Drupal\whitelabel\WhiteLabelNegotiatorInterface
   */
  protected $whiteLabelNegotiationManager;

  /**
   * NegotiationConfigureForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\whitelabel\WhiteLabelNegotiationMethodManager $whitelabel_negotiation_manager
   *   The white label negotiator.
   */
  public function __construct(ConfigFactoryInterface $config_factory, WhiteLabelNegotiationMethodManager $whitelabel_negotiation_manager) {
    parent::__construct($config_factory);
    $this->whiteLabelNegotiationManager = $whitelabel_negotiation_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.whitelabel_negotiation_manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'whitelabel_negotiation_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['whitelabel.negotiation'];
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;
    $form['#title'] = $this->t('Select negotiators');
    $form['description']['#markup'] = $this->t('Placeholder');

    $negotiators = $this->getAllNegotiators();

    $header = [
      t('Detection method'),
      t('Description'),
      t('Enabled'),
      t('Weight'),
    ];

    $form['negotiators'] = [
      '#type' => 'table',
      '#header' => $header,
      '#attributes' => ['id' => 'whitelabel-negotiation-methods'],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'whitelabel-method-weight',
        ],
      ],
    ];

    foreach ($negotiators as $negotiator_id => $negotiator) {
      $form['negotiators'][$negotiator_id]['#attributes']['class'][] = 'draggable';

      $form['negotiators'][$negotiator_id]['title'] = [
        '#prefix' => '<strong>',
        '#plain_text' => $negotiator->label(),
        '#suffix' => '</strong>',
      ];
      $form['negotiators'][$negotiator_id]['description'] = [
        '#plain_text' => $negotiator->getDescription(),
      ];
      $form['negotiators'][$negotiator_id]['enabled'] = [
        '#type' => 'checkbox',
        '#default_value' => $this->config('whitelabel.negotiation')->get("negotiator_settings.$negotiator_id.enabled") ? TRUE : FALSE,
      ];
      $form['negotiators'][$negotiator_id]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $negotiator->label()]),
        '#title_display' => 'invisible',
        '#default_value' => $this->config('whitelabel.negotiation')->get("negotiator_settings.$negotiator_id.weight") ?: 0,
        '#attributes' => ['class' => ['whitelabel-method-weight']],
      ];
    }

    // Add vertical tabs containing the settings for the processors. Tabs for
    // disabled processors are hidden with JS magic, but need to be included in
    // case the processor is enabled.
    $form['negotiator_settings'] = [
      '#title' => $this->t('Negotiator settings'),
      '#type' => 'vertical_tabs',
    ];
    foreach ($negotiators as $negotiator_id => $negotiator) {
      if ($negotiator instanceof PluginFormInterface) {
        $form['settings'][$negotiator_id] = [
          '#type' => 'details',
          '#title' => $negotiator->label(),
          '#group' => 'negotiator_settings',
          '#parents' => ['negotiators', $negotiator_id, 'settings'],
        ];
        $processor_form_state = SubformState::createForSubform($form['settings'][$negotiator_id], $form, $form_state);
        $form['settings'][$negotiator_id] += $negotiator->buildConfigurationForm($form['settings'][$negotiator_id], $processor_form_state);
      }
      else {
        unset($form['negotiator_settings'][$negotiator_id]);
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $values = $form_state->getValues();
    $negotiators = $this->getAllNegotiators();

    // Iterate over all processors that have a form and are enabled.
    foreach ($negotiators as $negotiator_id => $negotiator) {
      if (empty($values['negotiators'][$negotiator_id]['enabled'])) {
        continue;
      }

      if ($negotiator instanceof PluginFormInterface) {
        $processor_form_state = SubformState::createForSubform($form['settings'][$negotiator_id], $form, $form_state);
        $negotiator->validateConfigurationForm($form['settings'][$negotiator_id], $processor_form_state);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $negotiators = $this->getAllNegotiators();

    // Loop over all processors and save the settings of the enabled ones.
    foreach ($negotiators as $negotiator_id => $negotiator) {
      if (empty($values['negotiators'][$negotiator_id]['enabled'])) {
        // Do not process disabled negotiators.
        unset($values['negotiators'][$negotiator_id]);
        continue;
      }

      if ($negotiator instanceof PluginFormInterface) {
        $negotiator_form_state = SubformState::createForSubform($form['settings'][$negotiator_id], $form, $form_state);
        $negotiator->submitConfigurationForm($form['settings'][$negotiator_id], $negotiator_form_state);
      }
    }

    // Save configuration.
    $config_data = $values['negotiators'];
    $this->config('whitelabel.negotiation')
      ->set('negotiator_settings', $config_data)
      ->save();
  }

  /**
   * Helper function for fetching all negotiators.
   *
   * @return \Drupal\whitelabel\WhiteLabelNegotiationMethodInterface[]
   *   An array of white label negotiation methods.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  private function getAllNegotiators() {
    /** @var \Drupal\whitelabel\WhiteLabelNegotiationMethodManager $manager */
    $manager = $this->whiteLabelNegotiationManager;

    /** @var \Drupal\whitelabel\WhiteLabelNegotiationMethodInterface[] $processors */
    $processors = [];
    foreach ($manager->getDefinitions() as $name => $processor_definition) {
      $config = $this->config('whitelabel.negotiation')->get("negotiator_settings.$name.settings");
      $processor = $manager->createInstance($name, $config ?: []);
      $processors[$name] = $processor;
    }

    return $processors;
  }

}
