<?php

namespace Drupal\whitelabel;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginFormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Base class for white label negotiation methods.
 */
abstract class WhiteLabelNegotiationMethodBase extends PluginBase implements WhiteLabelNegotiationMethodInterface, PluginFormInterface, ConfigurableInterface {

  /**
   * The white label manager.
   *
   * @var \Drupal\whitelabel\WhiteLabelManagerInterface
   */
  protected $whiteLabelManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function setWhiteLabelManager(WhiteLabelManagerInterface $manager) {
    $this->whiteLabelManager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  abstract public function getWhiteLabel(Request $request = NULL);

  /**
   * {@inheritdoc}
   */
  public function label() {
    $plugin_definition = $this->getPluginDefinition();
    return $plugin_definition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $plugin_definition = $this->getPluginDefinition();
    return $plugin_definition['description'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
    return $this;
  }

}
