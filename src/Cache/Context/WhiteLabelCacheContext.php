<?php

namespace Drupal\whitelabel\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\whitelabel\WhiteLabelManagerInterface;

/**
 * Defines the WhiteLabelCacheContext service, for "per white label" caching.
 */
class WhiteLabelCacheContext implements CacheContextInterface {

  /**
   * Holds the white label.
   *
   * @var \Drupal\whitelabel\WhiteLabelManagerInterface
   */
  protected $whiteLabelManager;

  /**
   * WhiteLabelCacheContext constructor.
   *
   * @param \Drupal\whitelabel\WhiteLabelManagerInterface $white_label_manager
   *   The white label manager.
   */
  public function __construct(WhiteLabelManagerInterface $white_label_manager) {
    $this->whiteLabelManager = $white_label_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('White label');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    $white_label = $this->whiteLabelManager->getWhiteLabel();
    return !empty($white_label) ? $white_label->id() : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    $metadata = new CacheableMetadata();
    $whitelabel = $this->whiteLabelManager->getWhiteLabel();
    $metadata->addCacheableDependency($whitelabel);
    return $metadata;
  }

}
