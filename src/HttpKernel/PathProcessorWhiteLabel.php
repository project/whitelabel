<?php

namespace Drupal\whitelabel\HttpKernel;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\whitelabel\WhiteLabelManagerInterface;
use Drupal\whitelabel\WhiteLabelNegotiatorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The path processor for handling in- and outbound white label urls.
 *
 * The class is constructed for each request and delegates the in- and outbound
 * path processing to all plugins that support it.
 */
class PathProcessorWhiteLabel implements InboundPathProcessorInterface, OutboundPathProcessorInterface {

  /**
   * The white label manager.
   *
   * @var \Drupal\whitelabel\WhiteLabelManagerInterface
   */
  protected $whiteLabelManager;

  /**
   * The white label negotiator.
   *
   * @var \Drupal\whitelabel\WhiteLabelNegotiationMethodInterface
   */
  protected $negotiator;

  /**
   * The configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Local cache for white label path processors.
   *
   * @var array
   */
  protected $processors;

  /**
   * PathProcessorWhiteLabel constructor.
   *
   * @param \Drupal\whitelabel\WhiteLabelManagerInterface $white_label_manager
   *   The white label manager.
   * @param \Drupal\whitelabel\WhiteLabelNegotiatorInterface $negotiator
   *   The white label negotiatior.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The configuration.
   */
  public function __construct(WhiteLabelManagerInterface $white_label_manager, WhiteLabelNegotiatorInterface $negotiator, ConfigFactoryInterface $config) {
    $this->whiteLabelManager = $white_label_manager;
    $this->negotiator = $negotiator;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    if (!empty($path)) {
      $scope = 'inbound';
      if (!isset($this->processors[$scope])) {
        $this->initProcessors($scope);
      }
      foreach ($this->processors[$scope] as $instance) {
        $path = $instance->processInbound($path, $request);
      }
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    $this->negotiator->reset();
    $scope = 'outbound';
    if (!isset($this->processors[$scope])) {
      $this->initProcessors($scope);
    }
    foreach ($this->processors[$scope] as $instance) {
      $path = $instance->processOutbound($path, $options, $request, $bubbleable_metadata);
    }
    // No white label path allowed in this mode.
    if (empty($this->processors[$scope])) {
      unset($options['whitelabel']);
    }

    return $path;
  }

  /**
   * Initializes the local cache for language path processors.
   *
   * @param string $scope
   *   The scope of the processors: "inbound" or "outbound".
   */
  protected function initProcessors($scope) {
    $interface = '\Drupal\Core\PathProcessor\\' . Unicode::ucfirst($scope) . 'PathProcessorInterface';
    $this->processors[$scope] = [];
    $weights = [];
    foreach ($this->negotiator->getNegotiationMethods() as $method_id => $method) {
      if (!isset($this->processors[$scope][$method_id])) {
        $reflector = new \ReflectionClass($method['class']);
        if ($reflector->implementsInterface($interface)) {
          $this->processors[$scope][$method_id] = $this->negotiator->getNegotiationMethodInstance($method_id);
          $weights[$method_id] = $method['weight'];
        }
      }
    }

    // Sort the processors list, so that their functions are called in the
    // order specified by the weight of the methods.
    uksort($this->processors[$scope], function ($method_id_a, $method_id_b) use ($weights) {
      $a_weight = $weights[$method_id_a];
      $b_weight = $weights[$method_id_b];

      if ($a_weight == $b_weight) {
        return 0;
      }

      return ($a_weight < $b_weight) ? -1 : 1;
    });
  }

  /**
   * Resets the collected processors instances.
   */
  public function reset() {
    $this->processors = [];
  }

}
