<?php

namespace Drupal\whitelabel;

use Drupal\Core\Render\Element\RenderCallbackInterface;

/**
 * Provides a trusted callback to alter the system branding block.
 *
 * @see whitelabel_block_view_system_branding_block_alter()
 */
class WhiteLabelSystemBrandingBlockAlter implements RenderCallbackInterface {

  /**
   * Pre-render callback: Set visibility for site name in system branding block.
   *
   * Sets the site name visibility based on white label specific configuration.
   */
  public static function preRender(array $build) {
    /** @var \Drupal\whitelabel\Entity\WhiteLabelInterface $whitelabel */
    if ($whitelabel = \Drupal::service('whitelabel.whitelabel_manager')->getWhiteLabel()) {
      /** @var \Drupal\Core\Field\FieldItemListInterface $fields */
      $fields = $whitelabel->getFields(FALSE);

      if ($fields['name_display']->access()) {
        $build['content']['site_name']['#access'] = $whitelabel->getNameDisplay();
      }
    }
    return $build;
  }

}
