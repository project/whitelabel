<?php

namespace Drupal\whitelabel;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the white label entity.
 *
 * @see \Drupal\comment\Entity\WhiteLabel.
 */
class WhiteLabelAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account = NULL) {
    $account = $this->prepareUser($account);

    /** @var \Drupal\Core\Access\AccessResult $result */
    $result = parent::checkAccess($entity, $operation, $account);

    if ($result->isNeutral()) {
      // Owner can view and update with the right permissions.
      /** @var \Drupal\whitelabel\Entity\WhiteLabelInterface $entity */
      if (($operation == 'view' || $operation == 'update' || $operation == 'serve') && !empty($account) && $account->id() == $entity->getOwnerId()) {
        return AccessResult::allowedIfHasPermission($account, 'serve white label pages')->cachePerPermissions()->cachePerUser()->addCacheableDependency($entity);
      }

      // View access is independent of the white label, no cacheable dependency.
      if ($operation == 'view') {
        return AccessResult::allowedIfHasPermission($account, 'view white label pages')->cachePerPermissions();
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account = NULL, array $context = [], $entity_bundle = NULL) {
    /** @var \Drupal\Core\Access\AccessResult $result */
    // Have the admin permission checked by parent.
    $result = parent::checkCreateAccess($account, $context, $entity_bundle);
    if ($result->isNeutral()) {
      // No admin permission; check for serve permission.
      $result = AccessResult::allowedIfHasPermission($account, 'serve white label pages')->cachePerPermissions();
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    // Load white label configuration.
    $config = \Drupal::config('whitelabel.settings');

    if ($operation == 'view' || $operation == 'edit') {
      // White label tokens are always allowed.
      if ($field_definition->getName() == 'token') {
        return AccessResult::allowed()->setCacheMaxAge(0);
      }

      // Other fields are checked one by one.
      $fields = [
        'name_display',
        'name',
        'slogan',
        'logo',
        'theme',
      ];

      if (in_array($field_definition->getName(), $fields)) {
        if ($config->get('site_' . $field_definition->getName()) === TRUE) {
          return AccessResult::allowed()->addCacheableDependency($config);
        }
        else {
          return AccessResult::forbidden('Field not enabled in configuration.')->addCacheableDependency($config);
        }
      }
    }

    return parent::checkFieldAccess($operation, $field_definition, $account, $items);
  }

}
