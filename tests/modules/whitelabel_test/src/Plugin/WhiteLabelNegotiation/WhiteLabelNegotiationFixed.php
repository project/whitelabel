<?php

namespace Drupal\whitelabel_test\Plugin\WhiteLabelNegotiation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\whitelabel\Entity\WhiteLabel;
use Drupal\whitelabel\WhiteLabelNegotiationMethodBase;
use Drupal\whitelabel\WhiteLabelNegotiationMethodInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for selecting a pre-configured white label.
 *
 * @WhiteLabelNegotiation(
 *   id = \Drupal\whitelabel_test\Plugin\WhiteLabelNegotiation\WhiteLabelNegotiationFixed::METHOD_ID,
 *   label = @Translation("Fixed"),
 *   description = @Translation("Uses a fixed configurable white label."),
 *   weight = -10,
 * )
 */
class WhiteLabelNegotiationFixed extends WhiteLabelNegotiationMethodBase implements WhiteLabelNegotiationMethodInterface {

  use StringTranslationTrait;

  /**
   * The white label negotiation method id.
   */
  const METHOD_ID = 'fixed';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'whitelabel_id' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getWhiteLabel(Request $request = NULL) {
    // This test class is only intended to set a white label and not return it.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['whitelabel_id'] = [
      '#type' => 'number',
      '#title' => t('The white label ID to use.'),
      '#default_value' => $this->configuration['whitelabel_id'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $wid = $form_state->getValue('whitelabel_id');
    $whitelabel = \Drupal::entityQuery('whitelabel')->condition('wid', $wid)->execute();
    if (empty($whitelabel)) {
      $form_state->setErrorByName('whitelabel_id', t('The provided white label does not exist.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['whitelabel_id'] = $form_state->getValue('whitelabel_id');
  }

}
