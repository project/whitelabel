<?php

namespace Drupal\whitelabel_test\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for switching white label via URL. Used for testing.
 *
 * @package Drupal\whitelabel_test\Controller
 */
class WhiteLabelTestController extends ControllerBase {

  /**
   * Holds the white label provider.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * WhiteLabelPathProcessor constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Sets the white label to the white label of the provided token.
   *
   * @param string $test_value
   *   The value to set.
   *
   * @return array
   *   A render array.
   */
  public function set($test_value) {
    if ($test_value) {
      if ($test_value === 'unset') {
        $this->configFactory->getEditable('whitelabel.negotiation')
          ->set('negotiator_settings.fixed.settings.whitelabel_id', NULL)
          ->save();
      }

      if ($whitelabels = $this->entityTypeManager->getStorage('whitelabel')->loadByProperties(['token' => $test_value])) {
        $this->configFactory->getEditable('whitelabel.negotiation')
          ->set('negotiator_settings.fixed.settings.whitelabel_id', reset($whitelabels))
          ->save();
      }
    }

    return ['#markup' => $this->t('The current value of the stored session variable has been set to %val', ['%val' => $test_value])];
  }

}
