<?php

namespace Drupal\Tests\whitelabel\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\whitelabel\Cache\Context\WhiteLabelCacheContext;
use Drupal\whitelabel\Entity\WhiteLabel;
use Drupal\whitelabel\WhiteLabelManagerInterface;

/**
 * @coversDefaultClass \Drupal\whitelabel\Cache\Context\WhiteLabelCacheContext
 * @group whitelabel
 */
class WhitelLabelCacheContextTest extends UnitTestCase {

  /**
   * Tests 'whitelabel' cache context.
   */
  public function testWhiteLabelCacheContext() {
    $mock_white_label = $this->getMockBuilder(WhiteLabel::class)
      ->disableOriginalConstructor()
      ->getMock();
    $mock_white_label->expects($this->any())->method('id')->willReturn(3);

    $whiteLabelManager = $this->createMock(WhiteLabelManagerInterface::class);
    $whiteLabelManager->expects($this->any())->method('getWhiteLabel')->willReturn($mock_white_label);

    $whiteLabelCache = new WhiteLabelCacheContext($whiteLabelManager);
    $this->assertEquals('3', $whiteLabelCache->getContext());
    $this->assertEquals(['whitelabel:3'], $whiteLabelCache->getCacheableMetadata()->getCacheTags());
  }

}
