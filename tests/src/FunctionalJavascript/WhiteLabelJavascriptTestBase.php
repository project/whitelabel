<?php

namespace Drupal\Tests\whitelabel\FunctionalJavascript;

use Drupal\file\Entity\File;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\Tests\whitelabel\Traits\WhiteLabelCreationTrait;

/**
 * The Javascript base class for white label functional tests.
 *
 * This is the same as WhiteLabelTestBase, but extending JavascriptTestBase
 * instead.
 *
 * @package Drupal\Tests\whitelabel\FunctionalJavascript
 */
abstract class WhiteLabelJavascriptTestBase extends WebDriverTestBase {

  use WhiteLabelCreationTrait;
  use TestFileCreationTrait {
    getTestFiles as drupalGetTestFiles;
  }

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'bartik';

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'user',
    'image',
    'whitelabel',
  ];

  /**
   * Holds the white label owner.
   *
   * @var \Drupal\user\Entity\User
   */
  public $whiteLabelOwner;

  /**
   * Holds the white label.
   *
   * @var \Drupal\whitelabel\Entity\WhiteLabelInterface
   */
  public $whiteLabel;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->whiteLabelOwner = $this->drupalCreateUser(['serve white label pages']);
    $this->drupalLogin($this->whiteLabelOwner);

    $image_files = $this->drupalGetTestFiles('image');

    $this->whiteLabel = $this->createWhiteLabel([
      'token' => $this->randomMachineName(),
      'uid' => $this->whiteLabelOwner->id(),
      'name' => $this->randomString(),
      'name_display' => TRUE,
      'slogan' => $this->randomString(),
      'logo' => File::create((array) current($image_files)),
    ]);
  }

  /**
   * Function for determining if text is in the system branding block.
   *
   * @param string $text
   *   The text to look for.
   *
   * @throws \Behat\Mink\Exception\ElementTextException
   */
  public function inBrandingBlock($text) {
    $this->assertSession()->elementTextContains('css', '.block-system-branding-block', $text);
  }

  /**
   * Function for determining if text is not in the system branding block.
   *
   * @param string $text
   *   The text to look for.
   *
   * @throws \Behat\Mink\Exception\ElementTextException
   */
  public function notInBrandingBlock($text) {
    $this->assertSession()->elementTextNotContains('css', '.block-system-branding-block', $text);
  }

  /**
   * Function for determining if the given value is in the src attribute.
   *
   * @param string $src
   *   The value that should be present in the src attribute.
   *
   * @throws \Behat\Mink\Exception\ElementHtmlException
   */
  public function inImagePath($src) {
    $this->assertSession()->elementAttributeContains('css', '.block-system-branding-block img', 'src', $src);
  }

  /**
   * Function for determining if the given value is not in the src attribute.
   *
   * @param string $src
   *   The value that should not be present in the src attribute.
   *
   * @throws \Behat\Mink\Exception\ElementHtmlException
   */
  public function notInImagePath($src) {
    $this->assertSession()->elementAttributeNotContains('css', '.block-system-branding-block img', 'src', $src);
  }

}
