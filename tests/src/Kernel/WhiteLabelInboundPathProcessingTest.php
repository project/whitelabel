<?php

namespace Drupal\Tests\whitelabel\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\Tests\whitelabel\Traits\WhiteLabelCreationTrait;
use Drupal\whitelabel\Plugin\WhiteLabelNegotiation\WhiteLabelNegotiationUrl;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Tests if the correct style sheets are applied to a page.
 *
 * @group whitelabel
 */
class WhiteLabelInboundPathProcessingTest extends KernelTestBase {

  use WhiteLabelCreationTrait;
  use UserCreationTrait {
    createUser as drupalCreateUser;
  }

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'field',
    'text',
    'options',
    'user',
    'file',
    'image',
    'whitelabel',
    'node',
    'views',
  ];

  /**
   * Holds the generated white label throughout the different tests.
   *
   * @var \Drupal\whitelabel\Entity\WhiteLabelInterface
   */
  private $whiteLabel;

  /**
   * Holds the randomly generated token.
   *
   * @var string
   */
  private $token;

  /**
   * Holds the randomly generated query string parameter.
   *
   * @var string
   */
  private $queryStringIdentifier;

  /**
   * The PathProcessorManager.
   *
   * @var \Drupal\Core\PathProcessor\PathProcessorManager
   */
  private $pathProcessorManager;

  /**
   * The white label manager.
   *
   * @var \Drupal\whitelabel\WhiteLabelManagerInterface
   */
  protected $whiteLabelManager;

  /**
   * Constant that holds the new white label token.
   */
  const WHITE_LABEL_TOKEN = 'cool_whitelabel_token';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', ['sequences']);
    $this->installConfig(['whitelabel', 'user', 'field', 'node']);
    $this->installEntitySchema('whitelabel');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('view');

    $user = $this->drupalCreateUser(['serve white label pages']);
    $this->setCurrentUser($user);

    $this->token = self::WHITE_LABEL_TOKEN;
    $this->queryStringIdentifier = $this->randomMachineName(16);
    $this->whiteLabel = $this->createWhiteLabel(['token' => $this->token]);
    $this->whiteLabelManager = \Drupal::service('whitelabel.whitelabel_manager');
    $this->pathProcessorManager = $this->container->get('path_processor_manager');

    // Set the global white label configuration.
    $this->config('whitelabel.negotiation')
      ->set('negotiator_settings.url.enabled', TRUE)
      ->set('negotiator_settings.url.weight', 0)
      ->set('negotiator_settings.url.settings.domain', 'localhost')
      ->set('negotiator_settings.url.settings.query_string_identifier', $this->queryStringIdentifier)
      ->save();

    // Needed for correct resolving the system front page.
    $this->config('system.site')
      ->set('page.front', '/node')
      ->save();
  }

  /**
   * Test for the inbound query parameter mode.
   *
   * @dataProvider provideTokensBasicTests
   */
  public function testSimpleInboundRequests($mode, $token = NULL, $path = '/', $expect_whitelabel = TRUE, $expect_404 = FALSE) {
    // Query parameter mode will never throw an exception.
    if ($mode === WhiteLabelNegotiationUrl::CONFIG_QUERY_PARAMETER) {
      $expect_404 = FALSE;
    }

    // Ensure White Label is empty.
    $this->assertNull($this->whiteLabelManager->getWhiteLabel());

    // Set the test specific white label configuration.
    $this->config('whitelabel.negotiation')
      ->set('negotiator_settings.url.settings.mode', $mode)
      ->save();

    [$path, $request] = $this->generateRequest($mode, $token, $path);
    if ($expect_404) {
      $this->expectException(NotFoundHttpException::class);
    }
    $this->pathProcessorManager->processInbound($path, $request);

    // Ensure White Label equals the expected one.
    if ($expect_whitelabel) {
      $this->assertEquals($this->whiteLabel->id(), $this->whiteLabelManager->getWhiteLabel()->id());
    }
    else {
      $this->assertNull($this->whiteLabelManager->getWhiteLabel());
    }
  }

  /**
   * Data provider.
   *
   * @return array
   *   An array with a mode, token, expected white label and expected 404.
   */
  public function provideTokensBasicTests() {
    // Query parameter mode.
    $mode = WhiteLabelNegotiationUrl::CONFIG_QUERY_PARAMETER;
    // No token.
    $return[] = [$mode, NULL, '/', FALSE, FALSE];
    $return[] = [$mode, NULL, 'node', FALSE, FALSE];
    $return[] = [$mode, NULL, 'node/add', FALSE, FALSE];
    // Existing token.
    $return[] = [$mode, self::WHITE_LABEL_TOKEN, '/', TRUE, FALSE];
    $return[] = [$mode, self::WHITE_LABEL_TOKEN, 'node', TRUE, FALSE];
    $return[] = [$mode, self::WHITE_LABEL_TOKEN, 'node/add', TRUE, FALSE];
    // Non-existing token.
    // This combination can never cause an exception.
    $return[] = [$mode, 'someFakeToken', '/', FALSE, FALSE];
    $return[] = [$mode, 'someFakeToken', 'node', FALSE, FALSE];
    $return[] = [$mode, 'someFakeToken', 'node/add', FALSE, FALSE];

    // Path prefix mode.
    $mode = WhiteLabelNegotiationUrl::CONFIG_PATH_PREFIX;
    // No token.
    $return[] = [$mode, NULL, '/', FALSE, FALSE];
    $return[] = [$mode, NULL, 'node', FALSE, FALSE];
    $return[] = [$mode, NULL, 'node/add', FALSE, FALSE];
    // Existing token.
    $return[] = [$mode, self::WHITE_LABEL_TOKEN, '/', TRUE, FALSE];
    $return[] = [$mode, self::WHITE_LABEL_TOKEN, 'node', TRUE, FALSE];
    $return[] = [$mode, self::WHITE_LABEL_TOKEN, 'node/add', TRUE, FALSE];
    // Non-existing token.
    $return[] = [$mode, 'someFakeToken', '/', FALSE, TRUE];
    $return[] = [$mode, 'someFakeToken', 'node', FALSE, TRUE];
    $return[] = [$mode, 'someFakeToken', 'node/add', FALSE, TRUE];

    // Path prefix mode.
    $mode = WhiteLabelNegotiationUrl::CONFIG_DOMAIN;
    // No token.
    // This scenario does not exist.
    // Existing token.
    $return[] = [$mode, self::WHITE_LABEL_TOKEN, '/', TRUE, FALSE];
    $return[] = [$mode, self::WHITE_LABEL_TOKEN, 'node', TRUE, FALSE];
    $return[] = [$mode, self::WHITE_LABEL_TOKEN, 'node/add', TRUE, FALSE];
    // Non-existing token.
    $return[] = [$mode, 'someFakeToken', '/', FALSE, TRUE];
    $return[] = [$mode, 'someFakeToken', 'node', FALSE, TRUE];
    $return[] = [$mode, 'someFakeToken', 'node/add', FALSE, TRUE];

    return $return;
  }

  /**
   * Test to see if tokens based on system paths are correctly resolved.
   *
   * @dataProvider provideTokensTrickyTokenTests
   */
  public function testTrickyTokens($mode, $tricky_token, $tricky_path) {
    // Ensure White Label is empty.
    $this->assertNull($this->whiteLabelManager->getWhiteLabel());

    // Set the test specific white label configuration.
    $this->config('whitelabel.negotiation')
      ->set('negotiator_settings.url.settings.mode', $mode)
      ->save();

    $this->whiteLabel->setToken($tricky_token)->save();

    [$path, $request] = $this->generateRequest($mode, $tricky_token, $tricky_path);
    $this->pathProcessorManager->processInbound($path, $request);

    $this->assertEquals($this->whiteLabel->id(), $this->whiteLabelManager->getWhiteLabel()->id(), 'Asserting token: ' . $tricky_token . ' for path: ' . $tricky_path);
  }

  /**
   * Data provider.
   *
   * @return array
   *   An array with a mode, white label token and a path.
   */
  public function provideTokensTrickyTokenTests() {
    $modes = WhiteLabelNegotiationUrl::getModes();

    $tricky_tokens = [
      'admin',
      'node',
      'system',
      'user',
    ];

    $return = [];

    foreach (array_keys($modes) as $mode) {
      foreach ($tricky_tokens as $tricky_token) {
        foreach ($tricky_tokens as $tricky_path) {
          // Remove paths that do not serve a page.
          if (in_array($tricky_path, ['system'])) {
            break;
          }
          $return[] = [$mode, $tricky_token, $tricky_path];
        }
      }
    }
    return $return;
  }

  /**
   * Test to see if less privileged user gets a no WL page.
   *
   * @dataProvider provideModes
   */
  public function testViewerNoPermissions($mode) {
    // Test with unprivileged user.
    $white_label_viewer = $this->drupalCreateUser();
    $this->setCurrentUser($white_label_viewer);

    // Ensure that no White Label is provided.
    $this->assertNull($this->whiteLabelManager->getWhiteLabel());

    // Set the test specific white label configuration.
    $this->config('whitelabel.negotiation')
      ->set('negotiator_settings.url.settings.mode', $mode)
      ->save();

    [$path, $request] = $this->generateRequest($mode, $this->token);
    if ($mode == WhiteLabelNegotiationUrl::CONFIG_PATH_PREFIX || $mode == WhiteLabelNegotiationUrl::CONFIG_DOMAIN) {
      $this->expectException(NotFoundHttpException::class);
    }
    $this->pathProcessorManager->processInbound($path, $request);

    // Ensure White Label is empty.
    $this->assertNull($this->whiteLabelManager->getWhiteLabel());
  }

  /**
   * Test to see if less privileged owner serves a no WL page.
   *
   * @dataProvider provideModes
   */
  public function testOwnerNoPermissions($mode) {
    // Test with regular viewer account.
    $white_label_viewer = $this->drupalCreateUser(['view white label pages']);
    $this->setCurrentUser($white_label_viewer);

    // Change the owner of the white label to a less privileged one.
    $white_label_owner = $this->drupalCreateUser();
    $this->whiteLabel->setOwner($white_label_owner)->save();

    // Ensure that no White Label is provided.
    $this->assertNull($this->whiteLabelManager->getWhiteLabel());

    // Set the test specific white label configuration.
    $this->config('whitelabel.negotiation')
      ->set('negotiator_settings.url.settings.mode', $mode)
      ->save();

    [$path, $request] = $this->generateRequest($mode, $this->token);
    if ($mode == WhiteLabelNegotiationUrl::CONFIG_PATH_PREFIX || $mode == WhiteLabelNegotiationUrl::CONFIG_DOMAIN) {
      $this->expectException(NotFoundHttpException::class);
    }
    $this->pathProcessorManager->processInbound($path, $request);

    // Ensure White Label is empty.
    $this->assertNull($this->whiteLabelManager->getWhiteLabel());
  }

  /**
   * Data provider for tests.
   *
   * @return array
   *   Return an array with White label modes
   */
  public function provideModes() {
    return [
      [
        WhiteLabelNegotiationUrl::CONFIG_QUERY_PARAMETER,
      ],
      [
        WhiteLabelNegotiationUrl::CONFIG_PATH_PREFIX,
      ],
      [
        WhiteLabelNegotiationUrl::CONFIG_DOMAIN,
      ],
    ];
  }

  /**
   * Constructs an incoming request for the given mode and token.
   *
   * @param string $mode
   *   The white label mode to return the request for.
   * @param string|null $token
   *   The token to use when constructing the request, or NULL to use default.
   * @param string $path
   *   The path to take into account while generating the request.
   *
   * @return array
   *   An array with the uri and the Symfony\Component\HttpFoundation\Request.
   */
  protected function generateRequest($mode, $token = NULL, $path = '/') {
    switch ($mode) {
      case WhiteLabelNegotiationUrl::CONFIG_QUERY_PARAMETER:
        $request = Request::create($path, 'GET', [$this->queryStringIdentifier => $token]);
        break;

      case WhiteLabelNegotiationUrl::CONFIG_PATH_PREFIX:
        $path = empty($token) ? $path : $token . '/' . $path;
        $request = Request::create($path, 'GET');
        break;

      case WhiteLabelNegotiationUrl::CONFIG_DOMAIN:
        $http_host = empty($token) ? 'localhost' : $token . '.localhost';
        $request = Request::create($path, 'GET', [], [], [], ['HTTP_HOST' => $http_host]);
        break;
    }

    return [$path, $request];
  }

}
