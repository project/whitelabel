<?php

namespace Drupal\Tests\whitelabel\Kernel;

use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\Tests\whitelabel\Traits\WhiteLabelCreationTrait;
use Drupal\whitelabel\Plugin\WhiteLabelNegotiation\WhiteLabelNegotiationUrl;

/**
 * Tests to see if outbound urls contain the right token in the right place.
 *
 * @group whitelabel
 *
 * @todo Should assert what happens if the user does not have sufficient permissions. (Mitigated by inbound processor detecting it).
 */
class WhiteLabelOutboundPathProcessingTest extends KernelTestBase {

  use WhiteLabelCreationTrait;
  use UserCreationTrait {
    createUser as drupalCreateUser;
  }

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'text',
    'options',
    'user',
    'file',
    'image',
    'language',
    'whitelabel',
    'whitelabel_test',
  ];

  /**
   * Holds the generated white label throughout the different tests.
   *
   * @var \Drupal\whitelabel\Entity\WhiteLabelInterface
   */
  private $whiteLabel;

  /**
   * Holds the randomly generated token.
   *
   * @var string
   */
  private $token;

  /**
   * Holds the randomly generated query string parameter.
   *
   * @var string
   */
  private $queryStringIdentifier;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', ['sequences']);
    $this->installConfig(['language', 'whitelabel']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('whitelabel');

    ConfigurableLanguage::create(['id' => 'es'])->save();
    // This is somehow required...
    drupal_flush_all_caches();

    $user = $this->drupalCreateUser(['serve white label pages']);
    $this->setCurrentUser($user);

    $this->token = $this->randomMachineName(16);
    $this->queryStringIdentifier = $this->randomMachineName(16);
    $this->whiteLabel = $this->createWhiteLabel(['token' => $this->token]);

    $this->config('whitelabel.negotiation')
      ->set('negotiator_settings.url.enabled', TRUE)
      ->set('negotiator_settings.url.weight', 0)
      ->set('negotiator_settings.url.settings.query_string_identifier', $this->queryStringIdentifier)
      ->save();

    $this->setCurrentWhiteLabel($this->whiteLabel);
  }

  /**
   * Test white label URL creation.
   */
  public function testOutboundUrls() {
    $expected_patterns = [
      WhiteLabelNegotiationUrl::CONFIG_QUERY_PARAMETER => "http://localhost/?{$this->queryStringIdentifier}={$this->token}",
      WhiteLabelNegotiationUrl::CONFIG_PATH_PREFIX => "http://localhost/{$this->token}",
      WhiteLabelNegotiationUrl::CONFIG_DOMAIN => "http://{$this->token}.localhost/",
    ];

    foreach (array_keys($expected_patterns) as $mode) {
      // Configure the white label mode.
      $this->config('whitelabel.negotiation')
        ->set('negotiator_settings.url.settings.mode', $mode)
        ->save();
      // Build the URL.
      $url = Url::fromRoute('<front>')->setAbsolute();
      // Check if the white label is in the URL in the expected pattern.
      $this->assertStringContainsStringIgnoringCase($expected_patterns[$mode], $url->toString());
    }
  }

  /**
   * Test white label URL creation with the language module enabled.
   */
  public function testOutboundUrlsLanguage() {
    $this->config('language.negotiation')
      ->set('url.prefixes', ['en' => 'en', 'es' => 'es'])
      ->save();

    $expected_patterns = [
      WhiteLabelNegotiationUrl::CONFIG_QUERY_PARAMETER => "http://localhost/en?{$this->queryStringIdentifier}={$this->token}",
      WhiteLabelNegotiationUrl::CONFIG_PATH_PREFIX => "http://localhost/{$this->token}/en",
      WhiteLabelNegotiationUrl::CONFIG_DOMAIN => "http://{$this->token}.localhost/en",
    ];

    foreach (array_keys($expected_patterns) as $mode) {
      // Configure the white label mode.
      $this->config('whitelabel.negotiation')
        ->set('negotiator_settings.url.settings.mode', $mode)
        ->save();
      // Build the URL.
      $url = Url::fromRoute('<front>')->setAbsolute();
      // Check if the white label is in the URL in the expected pattern.
      $this->assertStringContainsStringIgnoringCase($expected_patterns[$mode], $url->toString());
    }
  }

}
