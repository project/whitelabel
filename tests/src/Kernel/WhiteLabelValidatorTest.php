<?php

namespace Drupal\Tests\whitelabel\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\user\Entity\User;
use Drupal\whitelabel\Entity\WhiteLabel;

/**
 * Tests that the white label not user name constraint works.
 *
 * @group whitelabel
 */
class WhiteLabelValidatorTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'text',
    'options',
    'user',
    'file',
    'image',
    'whitelabel',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', ['sequences']);
    $this->installSchema('file', ['file_usage']);
    $this->installConfig(['system', 'whitelabel']);
    $this->installEntitySchema('file');
    $this->installEntitySchema('user');
    $this->installEntitySchema('whitelabel');
  }

  /**
   * @covers \Drupal\whitelabel\Plugin\Validation\Constraint\WhiteLabelNotUsernameConstraint
   * @covers \Drupal\whitelabel\Plugin\Validation\Constraint\WhiteLabelNotUsernameConstraintValidator
   * @dataProvider getWhiteLabelData
   */
  public function testWhiteLabelNotUsernameConstraint($username, $token) {
    $user = User::create([
      'name' => $username,
      'status' => 1,
    ]);
    $user->save();

    // Create white label.
    $whitelabel = WhiteLabel::create(['token' => $token, 'uid' => $user->id()]);

    $result = $whitelabel->validate();

    if ($username === $token) {
      // Assert Fail.
      $this->assertCount(1, $result);
      $this->assertEquals('Due to security concerns this value cannot be the same as your user name.', (string) $result->get(0)->getMessage());
    }
    else {
      // Assert no error.
      $this->assertCount(0, $result);
    }
  }

  /**
   * Provides a list of file types to test.
   */
  public function getWhiteLabelData() {
    return [
      [
        'username' => 'same',
        'token' => 'same',
      ],
      [
        'username' => 'other',
        'token' => 'different',
      ],
    ];
  }

}
