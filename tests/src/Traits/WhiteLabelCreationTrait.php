<?php

namespace Drupal\Tests\whitelabel\Traits;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\whitelabel\Entity\WhiteLabel;
use Drupal\whitelabel\Entity\WhiteLabelInterface;

/**
 * Provides methods to create additional white label entities.
 */
trait WhiteLabelCreationTrait {

  /**
   * Sets the provided white label.
   *
   * @param \Drupal\whitelabel\Entity\WhiteLabelInterface $white_label
   *   The white label entity.
   */
  protected function setCurrentWhiteLabel(WhiteLabelInterface $white_label) {
    \Drupal::service('whitelabel.whitelabel_manager')->setWhiteLabel($white_label);
  }

  /**
   * Unsets the active white label.
   */
  protected function unsetWhiteLabel() {
    \Drupal::service('whitelabel.whitelabel_manager')->unsetWhiteLabel();
  }

  /**
   * Sets the provided white label for functional tests.
   *
   * @param \Drupal\whitelabel\Entity\WhiteLabelInterface $white_label
   *   The white label entity.
   */
  protected function setCurrentWhiteLabelFunctional(WhiteLabelInterface $white_label) {
    $config_factory = \Drupal::configFactory();
    $config_factory->getEditable('whitelabel.negotiation')
      ->set('negotiator_settings.fixed.enabled', TRUE)
      ->set('negotiator_settings.fixed.weight', -99)
      ->set('negotiator_settings.fixed.settings.whitelabel_id', $white_label->id())
      ->save();
  }

  /**
   * Unsets the active white label for functional tests.
   */
  protected function unsetWhiteLabelFunctional() {
    $config_factory = \Drupal::configFactory();
    $config_factory->getEditable('whitelabel.negotiation')
      ->set('negotiator_settings.fixed.enabled', TRUE)
      ->set('negotiator_settings.fixed.weight', -99)
      ->clear('negotiator_settings.fixed.settings.whitelabel_id')
      ->save();
  }

  /**
   * Creates a white label based on default settings.
   *
   * @param array $settings
   *   (optional) An associative array of settings for the white label, as used
   *   in entity_create(). Override the defaults by specifying the key and value
   *   in the array, for example:
   *
   *   @code
   *     $this->drupalCreateWhiteLabel(array(
   *       'token' => 'custom-token',
   *       'uid' => 2,
   *     ));
   *   @endcode
   *   The following defaults are provided:
   *   - token: Random string:
   *     @code
   *       $settings['token'] = $this->randomMachineName(),
   *     @endcode
   *   - uid: The currently logged in user, or anonymous.
   *
   * @return \Drupal\whitelabel\Entity\WhiteLabelInterface
   *   The created white label entity.
   */
  protected function createWhiteLabel(array $settings = []) {
    // Populate defaults array.
    $settings += [
      'token' => $this->randomMachineName(),
      'uid' => \Drupal::currentUser()->id(),
    ];
    /** @var \Drupal\whitelabel\Entity\WhiteLabelInterface $white_label */
    $white_label = WhiteLabel::create($settings);
    $white_label->save();

    return $white_label;
  }

  /**
   * Attaches a white label field to a given entity and bundle.
   *
   * @param string $entity_type
   *   The entity type to add the field to.
   * @param string $bundle
   *   The bundle to add the field to.
   * @param string $field_name
   *   The system name of the field.
   * @param string $field_label
   *   The label of the field.
   */
  public function attachFieldToEntity($entity_type, $bundle, $field_name = 'field_whitelabel', $field_label = 'White label') {
    // Create a white label field and attach it to a user.
    $field_storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'type' => 'entity_reference_revisions',
      'cardinality' => 1,
      'settings' => [
        'target_type' => 'whitelabel',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $bundle,
      'label' => $field_label,
    ]);
    $field->save();

    \Drupal::service('entity_display.repository')
      ->getFormDisplay($entity_type, $bundle, 'default')
      ->setComponent($field_name, [
        'type' => 'entity_reference_whitelabel',
        'settings' => [
          'color_scanner' => TRUE,
          'form_display_mode' => 'default',
        ],
      ])
      ->save();
  }

}
