<?php

namespace Drupal\Tests\whitelabel\Functional;

use Drupal\user\Entity\Role;

/**
 * Tests behavior for pages when user permissions change.
 *
 * @group whitelabel
 */
class WhiteLabelPermissionRevokeTest extends WhiteLabelTestBase {

  /**
   * Holds the site's default name (Drupal).
   *
   * @var string
   */
  private $defaultName;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'block',
    'user',
    'whitelabel',
    'whitelabel_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->defaultName = $this->config('system.site')->get('name');
  }

  /**
   * Test if white label is not applied once owner's permissions are revoked.
   */
  public function testOwnerPermissionsRevoked() {
    // Make sure test user can see white labels.
    $viewer = $this->drupalCreateUser(['view white label pages']);
    $this->drupalLogin($viewer);

    // Create a less privileged user and make it the white label owner.
    $user = $this->drupalCreateUser();
    $this->whiteLabel
      ->setOwner($user)
      ->save();
    $this->setCurrentWhiteLabelFunctional($this->whiteLabel);

    // Assert that white label is not applied.
    $this->drupalGet('<front>');
    $this->assertSession()->elementTextContains('css', 'title', $this->defaultName);

    // Create a role to show white labels and assign to user.
    $role = $this->drupalCreateRole(['serve white label pages']);
    $user->addRole($role);
    $user->save();

    // Assert that white label is applied.
    $this->drupalGet('<front>');
    $this->assertSession()->elementTextContains('css', 'title', $this->whiteLabel->getName());

    // Remove the role again.
    $user->removeRole($role);
    $user->save();

    // Assert white label is again not applied.
    $this->drupalGet('<front>');
    $this->assertSession()->elementTextContains('css', 'title', $this->defaultName);
  }

  /**
   * Test if white label is not applied once viewer's permissions are revoked.
   */
  public function testViewerPermissionsRevoked() {
    $viewer = $this->drupalCreateUser();
    $this->drupalLogin($viewer);
    $this->setCurrentWhiteLabelFunctional($this->whiteLabel);

    // Assert that the view white label permission was provided during install.
    /** @var \Drupal\user\RoleInterface $role */
    $role = Role::load(Role::AUTHENTICATED_ID);
    $this->assertTrue($role->hasPermission('view white label pages'), 'The Authenticated role has the "view white label pages" permission.');

    // Remove the permission.
    user_role_change_permissions(
      Role::AUTHENTICATED_ID,
      [
        'view white label pages' => FALSE,
      ]
    );
    $this->drupalGet('<front>');
    // Check page title, it should NOT contain the white label title.
    $this->assertSession()->elementTextContains('css', 'title', $this->defaultName);

    // Grant the permission.
    user_role_change_permissions(
      Role::AUTHENTICATED_ID,
      [
        'view white label pages' => TRUE,
      ]
    );
    $this->drupalGet('<front>');
    // Check page title, it should contain the white label title.
    $this->assertSession()->elementTextContains('css', 'title', $this->whiteLabel->getName());

    // Remove the permission.
    user_role_change_permissions(
      Role::AUTHENTICATED_ID,
      [
        'view white label pages' => FALSE,
      ]
    );
    $this->drupalGet('<front>');
    // Check page title, it should NOT contain the white label title.
    $this->assertSession()->elementTextContains('css', 'title', $this->defaultName);
  }

}
