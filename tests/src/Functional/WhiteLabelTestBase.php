<?php

namespace Drupal\Tests\whitelabel\Functional;

use Drupal\file\Entity\File;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\Tests\whitelabel\Traits\WhiteLabelCreationTrait;

/**
 * The base class for white label functional tests.
 *
 * Creates some sensible defaults and provides helper functions.
 *
 * @package Drupal\Tests\whitelabel\Functional
 */
abstract class WhiteLabelTestBase extends BrowserTestBase {

  use WhiteLabelCreationTrait;
  use TestFileCreationTrait {
    getTestFiles as drupalGetTestFiles;
  }

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'whitelabel',
    'user',
    'image',
  ];

  /**
   * Holds the white label owner.
   *
   * @var \Drupal\user\Entity\User
   */
  public $whiteLabelOwner;

  /**
   * Holds the white label.
   *
   * @var \Drupal\whitelabel\Entity\WhiteLabelInterface
   */
  public $whiteLabel;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->whiteLabelOwner = $this->drupalCreateUser(['serve white label pages']);
    $this->drupalLogin($this->whiteLabelOwner);

    $image_files = $this->drupalGetTestFiles('image');

    $this->whiteLabel = $this->createWhiteLabel([
      'token' => $this->randomMachineName(),
      'uid' => $this->whiteLabelOwner->id(),
      'name' => $this->randomString(),
      'name_display' => TRUE,
      'slogan' => $this->randomString(),
      'logo' => File::create((array) current($image_files)),
    ]);
  }

  /**
   * Function for determining if text is in the system branding block.
   *
   * @param string $text
   *   The text to look for.
   *
   * @throws \Behat\Mink\Exception\ElementTextException
   */
  public function inBrandingBlock($text) {
    $this->assertSession()->elementTextContains('css', '.block-system-branding-block', $text);
  }

  /**
   * Function for determining if text is not in the system branding block.
   *
   * @param string $text
   *   The text to look for.
   *
   * @throws \Behat\Mink\Exception\ElementTextException
   */
  public function notInBrandingBlock($text) {
    $this->assertSession()->elementTextNotContains('css', '.block-system-branding-block', $text);
  }

  /**
   * Function for determining if the given value is in the src attribute.
   *
   * @param string $src
   *   The value that should be present in the src attribute.
   *
   * @throws \Behat\Mink\Exception\ElementHtmlException
   */
  public function inImagePath($src) {
    $this->assertSession()->elementAttributeContains('css', '.block-system-branding-block img', 'src', $src);
  }

  /**
   * Function for determining if the given value is not in the src attribute.
   *
   * @param string $src
   *   The value that should not be present in the src attribute.
   *
   * @throws \Behat\Mink\Exception\ElementHtmlException
   */
  public function notInImagePath($src) {
    $this->assertSession()->elementAttributeNotContains('css', '.block-system-branding-block img', 'src', $src);
  }

}
