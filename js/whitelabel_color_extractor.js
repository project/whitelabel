/**
 * @file
 * Attaches the behaviors for the White Label module.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Displays the detected colors in the right foreground color.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attach color selection behavior to relevant context.
   */
  Drupal.behaviors.whitelabel_color_extractor = {
    attach: function (context, settings) {
      // This behavior attaches by ID, so is only valid once on a page.
      var div = $(context).find('.detected-colors').once('whitelabel_color_extractor');

      // Get the existing farbtastic.
      var farb = $.farbtastic('.color-placeholder');

      div.find('.detected-color').each(function() {
        var hex = $(this).text();
        callback($(this), $.trim(hex))
      });

      /**
       * Callback for Farbtastic when a new color is chosen.
       *
       * @param {HTMLElement} div
       *   The div.
       * @param {string} color
       *   The color that was chosen through the input.
       */
      function callback(div, color) {
        $(div).css({
          color: farb.RGBToHSL(farb.unpack(color))[2] > 0.5 ? '#000' : '#fff'
        });
      }

    }
  };

})(jQuery, Drupal);
