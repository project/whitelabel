/**
 * @file
 * Attaches the behaviors for the White Label module.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Displays farbtastic color selector and initialize white label
   * administration UI.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attach white label selection behavior to relevant context.
   */
  Drupal.behaviors.whitelabel_preview = {
    attach: function (context, settings) {

      $(context).find('.color-form').each(function() {
        var form = $(this);

        // Run the color preview callback for each
        Drupal.color.callback(context, settings, form);

        // The color preview callback only assumes one display per page, so the
        // logo will be changed for all white label previews on one page.
        // Because all theme's might name their logo preview different, select
        // them all by the global setting and then updaet them per instance.
        var logoElement = form.find('img[src="' + drupalSettings.color.logo + '"]');
        var logoSrc = form.find('input[name="logo"]').val();
        logoElement.attr('src', logoSrc);
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
